[[_TOC_]]


# Project overview:
This is VS 2019 .ASP.NET Core Web Application project, which uses  .NET Core 3.0 as target framework.
This Web APIs project performs CRUD operations for the Movies and provides a HTTP REST service.
The project implements a Git pipeline to build the Web Application as a Docker image on a Nginx web server and deploys this to a Kubernetes cluster running on an Azure virtual machine.

This project forms part of a Cloud Native initiative aim to build and share experience in Cloud Native technologies and Dev Ops practises.

The KubeMovieController project implements Web APIs of a multi-tier solution to manage a Movie catalog. The Movie catalog is maintained in a back-end repository [KubeMovieStore](https://gitlab.com/cnad/kubedvdstore) accessible via a Angular front-end  [KubeMovieAngular](https://gitlab.com/cnad/kubedvdangular).  

The project implements a Git pipeline to build the Web APIs application as a Docker image on a Nginx web server and deploys this to a Kubernetes cluster running on an Azure virtual machine.  

One of the  resources used for this project: https://code-maze.com/net-core-series/

This application uses System Environment variable to get connection to the Postgres database. So if you want to run application in the **Production** mode PostgreSQL must be installed and System environment variable with the name `PGConnectionString` must hold the  connection string to PostgreSQL in the following format: `ID=<user>;Password=<password>;Host=<host name>;Port=<port>;Database=<database name>;Pooling=true;`.

In the **Development** mode application will recreate Movie database and reseed the data on the every run of the application.

There is a `Movie.postman_collection.json` file in the `Notes` directory with the number of pre-set calls for `Postman`.

## Project  performs:
- CRUD operations
- Paging
- Filtering
- Searching
- Sorting
- Data Shaping
- HATEOAS
- Concurrency Support with ETag headers

## Project dependencies:
- AutoMapper - a convention-based object-object mapper.
- AutoMapper.Extensions.Microsoft.DependencyInjection
- Marvin.Cache.Headers - ASP.NET Core middleware that adds HttpCache headers to responses (Cache-Control, Expires, ETag, Last-Modified), and implements cache expiration & validation models
- NLog.Extensions.Logging -NLog Provider for Microsoft.Extensions.Logging
- EFCore.NamingConventions -  plugin to apply naming conventions to table and column names (e.g. snake_case)
- CassandraCSharpDriver -C# client library for Apache Cassandra and DataStax Enterprise
- Npgsql.EntityFrameworkCore.PostgreSQL - Entity Framework (EF) Core provider for PostgreSQL
- Newtonsoft.Json - for working with Json


## There are 3 different modes for running the application:
- Development - it uses SQLLite as data source
- Production - uses PostgreSQL as database (for this mode application requires System environment variable with the name `PGConnectionString` to be setup for the PostgreSQL connection - `ID=<user>;Password=<password>;Host=<host name>;Port=<port>;Database=<database name>;Pooling=true;`)
- Staging - uses Cassandra database

## Application performs the following operations

**GET**:

- `api/Movie/GetEnv?section=pgConnectionString` – Get value of the specific environment variable
- `api/Movie/GetGenres` – Get list of genres
- `api/Movie/GetGenresWithIds` – Get list of genres with Ids
- `api/Movie/GetMovies?genre=<genre name from the list>&title=<partial name>&pageNumber=2&pageSize=5&orderBy=<fields to order>&fields=<CSV list of short subset of fields to display>`  - Get list of Movies: 
•	genre - filters Movies by genre (exact match)
•	title - seraches Movies by Title (contains case insensitive)
•	pageNumber – displays specific page
•	pageSize – number of records per page
•	orderBy – field to be used for Order By criteria
•	fields – CSV list of fields to be displayed 

Example: `api/Movie?genre=superhero&title=2&pageNumber=1&pageSize=5&orderBy=title&fields=id,title`

- `api/Movie/GetMovie/<Movie Id>&fields=<CSV list of short subset of fields to display>`  - get specific Movie by Id and display all or limited number of fields
•	fields – CSV list of fields to be displayed
- `api/Movie/GetMoviesLinks?genre=<genre name from the list>&title=<partial name>&pageNumber=2&pageSize=5&orderBy=<fields to order>&fields=<CSV list of short subset of fields to display>`  - Get list of Movies as per HATEOAS: 
•	genre - filters Movies by genre (exact match)
•	title - seraches Movies by Title (contains case insensitive)
•	pageNumber – displays specific page
•	pageSize – number of records per page
•	orderBy – field to be used for Order By criteria
•	fields – CSV list of fields to be displayed 

**POST**:
- `api/Movie/CreateMovie` - adds Movie
Heders: 
[{"key":"Content-Type","value":"application/json","description":""}]
[{"key":"Accept","value":"application/json","description":""}]
Body:
{
	"Title" : "new Movie",
	"Genre": "action"
}

**PUT**:  
- `api/Movie/UpdateMovie/{id} - updates Movie with specified id
Heders: 
[{"key":"Content-Type","value":"application/json","description":""}]
[{"key":"Accept","value":"application/json","description":""}]
Body:
{
	"Title" : "new Movie",
	"Genre": "action"
}

**DELETE**: 
- `api/Movie/DeleteMovie/{id} - deletes Movie with specified id


# Passing multiple parameters to GET

- Create **ResourceParameters** class to pass different parameters (like filtering, sorting, etc)


`http://localhost:53922/api/Movie/GetMovies?genre=sciencefiction&title=Atlantis&pageNumber=1&pageSize=5&orderBy=title&fields=id,title`

This call requires:
- filter by genre "sciencefiction"
- search by title, which contains "Atlantis"
- ask for page number 1 with 5 records per page
- order collection by "title"
- display subset of data, which will include id and title fields only

![24-06-2020 12-13-55 PM.jpg](/KubeMovieController/Readme_Images/24-06-2020 12-13-55 PM.jpg?raw=true "")

````csharp
    // GET: api/Movie
    [HttpGet(Name = "GetMovies")]
    [ActionName("GetMovies")]
    public ActionResult GetMovies([FromQuery] MovieResourceParameters MovieResourceParameters)
````

<details><summary>Declare MovieResourceParameters class</summary>

<!-- have to be followed by an empty line! -->
````csharp
public class MovieResourceParameters
{
  public MovieResourceParameters()
  {
      PagingStateCassandra = null;
  }
  public string Genre { get; set; }
  public string Title { get; set; }

  const int maxPageSize = 100;

  public int PageNumber { get; set; } = 1;

  private int _pageSize = 5;
  public int PageSize
    {
      get => _pageSize;
      set => _pageSize = (value > maxPageSize) ? maxPageSize : value;
    }

​    public string OrderBy { get; set; } = "Title";
​    public string Fields { get; set; }

​    public byte[] PagingStateCassandra { get; set; }
  }
````
</details>

<details><summary>Perform sorting, filtering, etc inside Repository</summary>

<!-- have to be followed by an empty line! -->

````csharp
    public async Task<PagedList<Movie>> GetMoviesPaged(MovieResourceParameters MovieResourceParameters)
    {
      if (MovieResourceParameters == null)
      {
        throw new ArgumentNullException(nameof(MovieResourceParameters));
      }

      var collection = _context.Movie as IQueryable<Movie>;
      if (!string.IsNullOrWhiteSpace(MovieResourceParameters.Genre))
      {
        var categoryId = (int)(MovieGenre)Enum.Parse(typeof(MovieGenre), MovieResourceParameters.Genre.Trim(), true);
        collection = collection.Where(a => a.Category_id == categoryId);
      }
    
      if (!string.IsNullOrWhiteSpace(MovieResourceParameters.Title))
      {
    
        var searchQuery = MovieResourceParameters.Title.Trim();
        collection = collection.Where(a => a.Title.Contains(searchQuery));
      }
    
      if (!string.IsNullOrWhiteSpace(MovieResourceParameters.OrderBy))
      {
        // get property mapping dictionary
        var MoviePropertyMappingDictionary =
            _propertyMappingService.GetPropertyMapping<Models.MovieDto, Movie>();
    
        collection = collection.ApplySort(MovieResourceParameters.OrderBy,
            MoviePropertyMappingDictionary);
      }
    
      return PagedList<Movie>.Create(collection,
          MovieResourceParameters.PageNumber,
          MovieResourceParameters.PageSize);
    }
````
</details>

# Supporting Paging for Collection Resources
## What is Paging?
https://code-maze.com/paging-aspnet-core-webapi/

Paging refers to getting partial results from an API. Imagine having millions of results in the database and having your application try to return all of them at once.

Not only that would be an extremely ineffective way of returning the results, but it could also possibly have devastating effects on the application itself or the hardware it runs on. Moreover, every client has limited memory resources and it needs to restrict the number of shown results.

Thus, we need a way to return a set number of results to the client in order to avoid these consequences.
## Paging Implementation

What we want to achieve is something like this: `http://localhost:53922/api/Movie/GetMovies?pageNumber=2&pageSize=5`.This should return the second set of 5 Movies we have in our database.

<details><summary>We will use PagedList<T> class to return data from Repository</summary>

<!-- have to be followed by an empty line! -->

````csharp
  public class PagedList<T> : List<T>
  {
    public int CurrentPage { get; private set; }
    public int TotalPages { get; private set; }
    public int PageSize { get; private set; }
    public int TotalCount { get; private set; }
    public bool HasPrevious => (CurrentPage > 1);
    public bool HasNext => (CurrentPage < TotalPages);

    public byte[] PagingStateCassandra { get; set; }
    public PagedList(List<T> items, int count, int pageNumber, int pageSize)
    {
      TotalCount = count;
      PageSize = pageSize;
      CurrentPage = pageNumber;
      TotalPages = (int)Math.Ceiling(count / (double)pageSize);
      AddRange(items);
    }
    public static PagedList<T> Create(IQueryable<T> source, int pageNumber, int pageSize)
    {
      var count = source.Count();
      var items = source.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
      return new PagedList<T>(items, count, pageNumber, pageSize);
    }

  }
````
</details>

<details><summary>Add `X-Pagination` response header</summary>

<!-- have to be followed by an empty line! -->

````csharp
    [HttpGet(Name = "GetMovies")]
    [ActionName("GetMovies")]
    public ActionResult GetMovies([FromQuery] MovieResourceParameters MovieResourceParameters)
    {

      if (!_propertyMappingService.ValidMappingExistsFor<MovieDto, Entities.Movie>
    (MovieResourceParameters.OrderBy))
      {
        return BadRequest();
      }
      if (!_propertyCheckerService.TypeHasProperties<MovieDto>
              (MovieResourceParameters.Fields))
      {
        return BadRequest();
      }
      var MoviesFromRepo = _kubeMovieRepository.GetMoviesPaged(MovieResourceParameters).Result;

      var previousPageLink = MoviesFromRepo.HasPrevious ?
          CreateMoviesResourceUri(MovieResourceParameters,
          ResourceUriType.PreviousPage) : null;

      var nextPageLink = MoviesFromRepo.HasNext ?
          CreateMoviesResourceUri(MovieResourceParameters,
          ResourceUriType.NextPage) : null;
      
      //need to supply pagingState for Cassandra (in object or in header)
      byte[]  pagingState = new byte[] { 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20 };
      var paginationMetadata = new
      {
        totalCount = MoviesFromRepo.TotalCount,
        pageSize = MoviesFromRepo.PageSize,
        currentPage = MoviesFromRepo.CurrentPage,
        totalPages = MoviesFromRepo.TotalPages,
        previousPageLink,
        nextPageLink,
        pagingState = pagingState
      };

      Response.Headers.Add("X-Pagination",
          JsonSerializer.Serialize(paginationMetadata));

      return Ok(_mapper.Map<IEnumerable<MovieDto>>(MoviesFromRepo)
          .ShapeData(MovieResourceParameters.Fields));
    }
````
</details>

<details><summary>CreateMoviesResourceUri</summary>

<!-- have to be followed by an empty line! -->

````csharp
    private string CreateMoviesResourceUri(MovieResourceParameters MovieResourceParameters, ResourceUriType type)
    {
      switch (type)
      {
        case ResourceUriType.PreviousPage:
          return Url.Link("GetMovies",
            new
            {
              fields = MovieResourceParameters.Fields,
              orderBy = MovieResourceParameters.OrderBy,
              pageNumber = MovieResourceParameters.PageNumber - 1,
              pageSize = MovieResourceParameters.PageSize,
              genre = MovieResourceParameters.Genre,
              title = MovieResourceParameters.Title
            });
        case ResourceUriType.NextPage:
          return Url.Link("GetMovies",
            new
            {
              fields = MovieResourceParameters.Fields,
              orderBy = MovieResourceParameters.OrderBy,
              pageNumber = MovieResourceParameters.PageNumber + 1,
              pageSize = MovieResourceParameters.PageSize,
              genre = MovieResourceParameters.Genre,
              title = MovieResourceParameters.Title
            });

        default:
          return Url.Link("GetMovies",
          new
          {
            fields = MovieResourceParameters.Fields,

            orderBy = MovieResourceParameters.OrderBy,
            pageNumber = MovieResourceParameters.PageNumber,
            pageSize = MovieResourceParameters.PageSize,
            genre = MovieResourceParameters.Genre,
            title = MovieResourceParameters.Title
          });
      }

    }
````
</details>

<details><summary>CreateLinksForMovies</summary>

<!-- have to be followed by an empty line! -->

````csharp
    private IEnumerable<LinkDto> CreateLinksForMovies(MovieResourceParameters MovieResourceParameters, bool hasNext, bool hasPrevious)    
    {
      var links = new List<LinkDto>();

      // self 
      links.Add(
         new LinkDto(CreateMoviesResourceUri(
             MovieResourceParameters, ResourceUriType.Current)
         , "self", "GET"));

      if (hasNext)
      {
        links.Add(
          new LinkDto(CreateMoviesResourceUri(
              MovieResourceParameters, ResourceUriType.NextPage),
          "nextPage", "GET"));
      }

      if (hasPrevious)
      {
        links.Add(
            new LinkDto(CreateMoviesResourceUri(
                MovieResourceParameters, ResourceUriType.PreviousPage),
            "previousPage", "GET"));
      }

      return links;
    }
````
</details>

![24-06-2020 11-49-36 AM.jpg](/KubeMovieController/Readme_Images/24-06-2020 11-49-36 AM.jpg?raw=true "")

# Implementing HATEOAS

https://code-maze.com/hateoas-aspnet-core-web-api/
### What is HATEOAS
HATEOAS (Hypermedia as the Engine of Application State) is a very important REST constraint. Without it, a REST API cannot be considered RESTful and many of the benefits we get by implementing a REST architecture are unavailable.

Hypermedia refers to any kind of content that contains links to media types such as documents, images, videos…

REST architecture allows us to generate hypermedia links in our responses dynamically and thus make navigation much easier. To put this into perspective, think about a website that uses hyperlinks to help you navigate to different parts of it. You can achieve the same effect with HATEOAS in your REST API.

**REST** response body for `http://localhost:53922/api/Movie/GetMovies?orderby = id`
````json
    {
        "id": 18,
        "title": "24: Season Six preview",
        "genre": "Action",
        "categoryId": 104,
        "exampleProperty": "18:24: Season Six preview"
    }
````
**HATEOAS** response body for `http://localhost:53922/api/Movie/GetMoviesLinks?orderby = id`
````json
        {
            "id": 18,
            "title": "24: Season Six preview",
            "genre": "Action",
            "categoryId": 104,
            "exampleProperty": "18:24: Season Six preview",
            "links": [
                {
                    "href": "http://localhost:53922/api/Movie/GetMovie/18",
                    "rel": "self",
                    "method": "GET"
                },
                {
                    "href": "http://localhost:53922/api/Movie/DeleteMovie/18",
                    "rel": "delete_Movie",
                    "method": "DELETE"
                },
                {
                    "href": null,
                    "rel": "create_Movie",
                    "method": "POST"
                },
                {
                    "href": "http://localhost:53922/api/Movie/UpdateMovie/18",
                    "rel": "update_Movie",
                    "method": "PUT"
                }
            ]
        }
````
### What is a Link?
According to **RFC5988**, a link is “a typed connection between two resources that are identified by **Internationalised Resource Identifiers (IRIs)**“. Simply put we use links to traverse the internet or rather the resources on the internet.

Our responses contain an array of Links, which consist of a few properties according to the RFC:

- href – represents a target URI
- rel – represents a link relation type, which means it describes how the current context is related to the target resource
- method – we need an HTTP method to know how to distinguish the same target URIs
### Pros/Cons of Implementing HATEOAS
So what are all the benefits we can expect when implementing HATEOAS?

HATEOAS is not trivial to implement, but the rewards we reap are worth it. Some of the things we can expect to get when we implement HATEOAS:

- API becomes self-discoverable and explorable
- A client can use the links to implement its logic, it becomes much easier, and any changes that happen in the API structure are directly reflected onto the client
- The server drives the application state and URL structure and not vice versa
- The link relations can be used to point to developer documentation
- Versioning through hyperlinks becomes easier
- Reduced invalid state transaction calls
- API is evolvable without breaking all the clients

How to implement links:
<details><summary>Create the PagedList class that will handle pagination</summary>

<!-- have to be followed by an empty line! -->

````csharp
public class PagedList<T> : List<T>
  {
    public int CurrentPage { get; private set; }
    public int TotalPages { get; private set; }
    public int PageSize { get; private set; }
    public int TotalCount { get; private set; }
    public bool HasPrevious => (CurrentPage > 1);
    public bool HasNext => (CurrentPage < TotalPages);

    public byte[] PagingStateCassandra { get; set; }
    public PagedList(List<T> items, int count, int pageNumber, int pageSize)
    {
      TotalCount = count;
      PageSize = pageSize;
      CurrentPage = pageNumber;
      TotalPages = (int)Math.Ceiling(count / (double)pageSize);
      AddRange(items);
    }
    public static PagedList<T> Create(IQueryable<T> source, int pageNumber, int pageSize)
    {
      var count = source.Count();
      var items = source.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
      return new PagedList<T>(items, count, pageNumber, pageSize);
    }

  }
````
</details>

<details><summary>Update Repository to return PagedList</summary>

<!-- have to be followed by an empty line! -->

````csharp
      return PagedList<Movie>.Create(collection,
          MovieResourceParameters.PageNumber,
          MovieResourceParameters.PageSize);
````
</details>

<details><summary>Update Controller to produce links</summary>

<!-- have to be followed by an empty line! -->

````csharp
    // GET: api/Movie
    [HttpGet(Name = "GetMoviesLinks")]
    [ActionName("GetMoviesLinks")]
    public ActionResult GetMoviesLinks([FromQuery] MovieResourceParameters MovieResourceParameters)
    {

      if (!_propertyMappingService.ValidMappingExistsFor<MovieDto, Entities.Movie>
    (MovieResourceParameters.OrderBy))
      {
        return BadRequest();
      }
      if (!_propertyCheckerService.TypeHasProperties<MovieDto>
              (MovieResourceParameters.Fields))
      {
        return BadRequest();
      }
      var MoviesFromRepo = _kubeMovieRepository.GetMoviesPaged(MovieResourceParameters).Result;

      var paginationMetadata = new
      {
        totalCount = MoviesFromRepo.TotalCount,
        pageSize = MoviesFromRepo.PageSize,
        currentPage = MoviesFromRepo.CurrentPage,
        totalPages = MoviesFromRepo.TotalPages
      };

      Response.Headers.Add("X-Pagination",
          JsonSerializer.Serialize(paginationMetadata));

      var links = CreateLinksForMovies(MovieResourceParameters,
          MoviesFromRepo.HasNext,
          MoviesFromRepo.HasPrevious);

      var shapedMovies = _mapper.Map<IEnumerable<MovieDto>>(MoviesFromRepo)
                         .ShapeData(MovieResourceParameters.Fields);

      var shapedMoviesWithLinks = shapedMovies.Select(Movie =>
      {
        var MovieAsDictionary = Movie as IDictionary<string, object>;
        var MovieLinks = CreateLinksForMovie((int)MovieAsDictionary["Id"], null);
        MovieAsDictionary.Add("links", MovieLinks);
        return MovieAsDictionary;
      });

      var linkedCollectionResource = new
      {
        value = shapedMoviesWithLinks,
        links
      };

      return Ok(linkedCollectionResource);

    }

````csharp
    private IEnumerable<LinkDto> CreateLinksForMovie(int MovieId, string fields)
    {

      var links = new List<LinkDto>();

      if (string.IsNullOrWhiteSpace(fields))
      {
        links.Add(
          new LinkDto(Url.Link("GetMovie", new { id = MovieId }),
          "self",
          "GET"));
      }
      else
      {
        links.Add(
          new LinkDto(Url.Link("GetMovie", new { id = MovieId, fields }),
          "self",
          "GET"));
      }

      links.Add(
         new LinkDto(Url.Link("DeleteMovie", new { id = MovieId }),
         "delete_Movie",
         "DELETE"));

      links.Add(
          new LinkDto(Url.Link("CreateMovie", new { id = MovieId }),
          "create_Movie",
          "POST"));

      links.Add(
         new LinkDto(Url.Link("UpdateMovie", new { id = MovieId }),
         "update_Movie",
         "PUT"));

      return links;
    }

  }
````
</details>

# Tips

## Case of multiple GET calls in the controller

<details><summary>Use both HttpGet(Name = "MethodName") and  ActionName("MethodName") attributes</summary>

<!-- have to be followed by an empty line! -->

````csharp

  [Route("api/[controller]/[action]")]
  [ApiController]

  public class MovieController : ControllerBase

    // GET: api/Movie/GetGenres    
    [HttpGet(Name = "GetGenres")]
    [ActionName("GetGenres")]
    public ActionResult GetGenres()
    {
      ...
    }

    // GET: api/Movie/GetGenres    
    [HttpGet(Name = "GetGenres")]
    [ActionName("GetGenres")]
    public ActionResult GetGenres()
    {
      ...
    }
````
</details>

## To map objects with AutoMapper:

<details><summary>Create required Profile in the Profiles folder</summary>

<!-- have to be followed by an empty line! -->

````csharp
namespace KubeMovieController.Profiles
{
  public class AutoMapperProfile : Profile
  {
    public AutoMapperProfile()
    {
      CreateMap<Entities.Movie, Models.MovieDto>()
          .ForMember(
              dest => dest.CategoryId,
              opt => opt.MapFrom(src => src.Category_id))
           .ForMember(
              dest => dest.ExampleProperty,
              opt => opt.MapFrom(src => string.Concat(src.Id, ":", src.Title)));


      CreateMap<Models.MovieForCreationDto, Entities.Movie>()
        .ForMember(
              dest => dest.Category_id, opt => opt.MapFrom(src => (MovieGenre)System.Enum.Parse(typeof(MovieGenre), src.Genre,true))); 
      CreateMap<Models.MovieForUpdateDto, Entities.Movie>()
           .ForMember(
              dest => dest.Category_id, opt => opt.MapFrom(src => (MovieGenre)System.Enum.Parse(typeof(MovieGenre), src.Genre,true)));
      CreateMap<Entities.Movie, Models.MovieForUpdateDto>()
           .ForMember(
              dest => dest.Genre, opt => opt.MapFrom(src => Enum.GetName(typeof(MovieGenre), src.Category_id)));

    }

  }
}
````
</details>

- Where required use:
````csharp
  var MovieToReturn = _mapper.Map<MovieDto>(MovieEntity);
````
## Version control

Vendor-specific Media Types can be used as a mean of the specific version of the Web API. In the Post method `application/vnd.marvin.movieforcreation+json` header is used for the example
````csharp
    [RequestHeaderMatchesMediaType("Content-Type",
            "application/json",
            "application/vnd.marvin.movieforcreation+json")]
    [Consumes("application/json",
            "application/vnd.marvin.movieforcreation+json")]
````
# Git CI/CD pipelines

Pipelines are the top-level component of continuous integration, delivery, and deployment.

Pipelines comprise:

- Jobs, which define *what* to do. For example, jobs that compile or test code.

- Stages, which define *when* to run the jobs. For example, stages that run tests after stages that compile the code.

The pipeline provides the automation of build and deployment scripts whenever there is an update to the master branch. So with any commit or typically with a merge from a feature branch the GitLab CI/CD pipeline is triggered into action. The pipeline is implemented here using the following scripts. The pipeline currently comprises build and deployment stages. 

* .gitlab-ci.yml, 
* Dockerfile, 
* deployment.yml, 
* ingresscontroller.yaml

The pipelines are driven by `.gitlab-ci.yml` file, which must be present in the root folder in Git.

## .gitlab-ci.yml
A [Basic Gitlab Pipeline](https://docs.gitlab.com/ee/ci/pipelines/pipeline_architectures.html) is implemented here, and is simplified into two stages; a build stage and a deploy stage. Everything is run in the build stage concurrently, then everything is run in the deploy stage.  
The Gitlab CI/CD pipeline is configured with the .gitlab-ci.yml YAML file. This file is based on a .gitlab-ci.yml [Docker Template](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Docker.gitlab-ci.yml) for the build stage; one of the many [example templates](https://docs.gitlab.com/ee/ci/examples/#cicd-templates)  

<details><summary>.gitlab-ci.yml</summary>

```docker-build: 
image: mcr.microsoft.com/dotnet/core/sdk:3.1

stages:
    - build
    - test
    - build_two
    - deploy
    
variables:
  project: "KubeMovieController"
    
build:
    stage: build
    script:
      - "dotnet restore $project/KubeMovieController.csproj"
      - "dotnet build -c Release $project/KubeMovieController.csproj"
    artifacts:
      paths: 
        - /builds/cnad/kubedvdcontroller/KubeMovieController/bin/Development/netcoreapp3.1/KubeMovieController.dll

test:
    stage: test
    script: 
      - "dotnet restore $project/KubeMovieController.csproj"
      - "cd .."
      - "dotnet test  kubedvdcontroller/KubeMovieController.Test/KubeMovieController.Test.csproj"

docker-build:
    # Official docker image.
    image: docker:latest
    stage: build_two
    services:
    - docker:dind
    before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    script:
    - docker build --build-arg PG_ARG="$PGSECRET" --build-arg VERSION_ARG="$CI_COMMIT_SHORT_SHA" -t "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG" -t "$CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA" -t "$CI_REGISTRY_IMAGE:latest" .    
    - docker push "$CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA"
    - docker push "$CI_REGISTRY_IMAGE:latest"

docker-deploy:
    image: 
      name: cnad/kubectl:latest
      entrypoint: [""]
    stage: deploy
    script:
      - export KUBECONFIG=$kube_config
      - kubectl apply -f deployment.yaml 
      - kubectl rollout restart deployments/dvdimagegit 
      - kubectl apply -f ingresscontroller.yaml 
    environment:
      name: staging
      url: https://52.170.3.117:16443
     

```

</details>

For this application pipeline include 4 jobs:

1. Build the c# application
1. Perform the unit tests
    The difficult part  setting up first 2 jobs - the correct directories

1. The docker-build step mostly follows the template with some minor additions
    The docker-build mostly follows the template with some minor additions
    * build - multiple tags are applied to the built image as follows (whereas the template does not tag the image):
	    * $CI_COMMIT_REF_SLUG
	    * $CI_COMMIT_SHORT_SHA
	    * latest

	    The CI_ variables are [predefined Gitlab environment variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html) used to identify the commit.

	    Additionally, the build-arg option is used to set build-time variables which can be referenced from the Dockerfile. This is used to update the image environment with a Gitlab Commit reference, that is used to identify the source of the runtime image. Refer Dockerfile later...

    * push - the image tagged latest is pushed (whereas the template does not push tagged image)
    
    ````
    - docker push "$CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA"
    ````
    
    This line pushes a docker image that contains the first six characters of the GitLab commit id.


1. The docker-deploy step the built image is deployed to a [Kubernetes cluster](reference to the Kubernetes cluster goes here) using kubectl command and deployment YAML. The kubectl command is made available via the [cnad/kubectl](https://gitlab.com/cnad/kubectl) Docker image.  

GitLab’s CI/CD variables are used to store cluster authentication information which is made available to kubectl via the KUBECONFIG environment variable; the environment variable is set as part of the docker-deploy job. The deployment YAML (deployment.yaml) is then used to deploy the Web front end image to the Kubernetes cluster, restart the resource and to configure the cluster Ingress.  

We use a blank image of Kubernetes created in another `cnad\kubectl` project and we specify a blank entry point to prevent GitLab CLI runner from performing any automatic code

````docker
  name: cnad/kubectl:latest
  entrypoint: [""]
````

And then we:
- apply our Kubernetes configuration from the cluster

````docker
- export KUBECONFIG=$kube_config
````
- use the `deployment.yaml` to deploy our docker image
````docker
- kubectl apply -f deployment.yaml 
````
- restart the deployment so it will rollout all changes to all pods 
````docker
- kubectl rollout restart deployments/dvdimagegit 
````
- set an ingress to expose our controller externally 
````docker
- kubectl apply -f ingresscontroller.yaml 
````
## deployment.yaml
Contains Load Balancer Service and single POD endpoint 

## ingresscontroller.yaml
Configure Kubernetes Nginx Ingress Controller 


# Future considerations

- CORS - at the moment we use unlimited CORS policy. This must be improved for the production grade app.


````csharp
services.AddCors(options =>
{
  options.AddPolicy("CorsPolicy",
      builder => builder.AllowAnyOrigin()
          .AllowAnyMethod()
          .AllowAnyHeader());
});
````

- HATEOS - extend of future usage
- Caching  (extent of usage)
- Advanced Content Negotiation via vendor-specific media types on input and output