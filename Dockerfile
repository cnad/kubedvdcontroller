FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /src
COPY ["KubeMovieController/KubeMovieController.csproj", ""]
RUN dotnet restore "./KubeMovieController.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet build "KubeMovieController/KubeMovieController.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "KubeMovieController/KubeMovieController.csproj" -c Release -o /app/publish

FROM base AS final
ARG PG_ARG=0
ARG VERSION_ARG=0

WORKDIR /app
ENV PGConnectionString=$PG_ARG
ENV KubeMovieControllerVersion=$VERSION_ARG

COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "KubeMovieController.dll"]
