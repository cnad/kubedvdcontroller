﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KubeMovieController.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Movie",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Title = table.Column<string>(maxLength: 100, nullable: false),
                    Category_id = table.Column<int>(nullable: false),
                    Media_type_id = table.Column<int>(nullable: false),
                    Discs = table.Column<int>(nullable: false),
                    Region_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movie", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Movie",
                columns: new[] { "Id", "Category_id", "Discs", "Media_type_id", "Region_id", "Title" },
                values: new object[] { 1, 101, 0, 0, 0, "Movie Title 1" });

            migrationBuilder.InsertData(
                table: "Movie",
                columns: new[] { "Id", "Category_id", "Discs", "Media_type_id", "Region_id", "Title" },
                values: new object[] { 2, 102, 0, 0, 0, "Movie Title 2" });

            migrationBuilder.InsertData(
                table: "Movie",
                columns: new[] { "Id", "Category_id", "Discs", "Media_type_id", "Region_id", "Title" },
                values: new object[] { 3, 104, 0, 0, 0, "Movie Title 3" });

            migrationBuilder.InsertData(
                table: "Movie",
                columns: new[] { "Id", "Category_id", "Discs", "Media_type_id", "Region_id", "Title" },
                values: new object[] { 4, 104, 0, 0, 0, "Movie Title 4" });

            migrationBuilder.InsertData(
                table: "Movie",
                columns: new[] { "Id", "Category_id", "Discs", "Media_type_id", "Region_id", "Title" },
                values: new object[] { 5, 105, 0, 0, 0, "Movie Title 5" });

            migrationBuilder.InsertData(
                table: "Movie",
                columns: new[] { "Id", "Category_id", "Discs", "Media_type_id", "Region_id", "Title" },
                values: new object[] { 6, 106, 0, 0, 0, "Movie Title 6" });

            migrationBuilder.InsertData(
                table: "Movie",
                columns: new[] { "Id", "Category_id", "Discs", "Media_type_id", "Region_id", "Title" },
                values: new object[] { 7, 101, 0, 0, 0, "Movie Title 7" });

            migrationBuilder.InsertData(
                table: "Movie",
                columns: new[] { "Id", "Category_id", "Discs", "Media_type_id", "Region_id", "Title" },
                values: new object[] { 8, 102, 0, 0, 0, "Movie Title 12" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Movie");
        }
    }
}
