﻿using Cassandra;
using Cassandra.Mapping;
using KubeMovieController.DAO;
using KubeMovieController.Entities;
using KubeMovieController.Helpers;
using KubeMovieController.Logging;
using KubeMovieController.ResourceParameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KubeMovieController.Services
{
  public class KubeMovieRepository : IKubeMovieRepository
  {
    protected readonly ISession session;
    protected readonly IMapper mapper;
    private readonly IPropertyMappingService _propertyMappingService;
    private ILog _logger;
    public KubeMovieRepository(ICassandraDAO cassandraDAO,
            IPropertyMappingService propertyMappingService,
            ILog logger)
    {
      //ICassandraDAO cassandraDAO = new CassandraDAO();
      session = cassandraDAO.GetSession();
      mapper = new Mapper(session);
      _propertyMappingService = propertyMappingService ??
               throw new ArgumentNullException(nameof(propertyMappingService));
      _logger = logger ??
              throw new ArgumentNullException(nameof(logger));
    }
    public List<string> GetGenres()
    {
      return ((MovieGenre[])Enum.GetValues(typeof(MovieGenre))).Select(c => c.ToString()).ToList();
    }
    public Dictionary<int, string> GetGenresWithIds()
    {
      return ((MovieGenre[])Enum.GetValues(typeof(MovieGenre))).ToDictionary(v => (int)v, k => k.ToString());
    }
    public async Task<IEnumerable<Movie>> GetMovies()
    {
      return await mapper.FetchAsync<Movie>();
    }
    public async Task<PagedList<Movie>> GetMoviesPaged(MovieResourceParameters movieResourceParameters)
    {
      if (movieResourceParameters == null)
      {
        throw new ArgumentNullException(nameof(movieResourceParameters));
      }

      string sql = ConstructSql(movieResourceParameters);
      byte[] pagingStateCassandra = movieResourceParameters.PagingStateCassandra;
      RowSet result = ExecuteCommandWithPageSize(sql, movieResourceParameters.PageSize, ref pagingStateCassandra);

      int count = GetMovieRecordCount();
      PagedList<Movie> resultPaged = new PagedList<Movie>((result as IEnumerable<Movie>).ToList(), count, movieResourceParameters.PageNumber, movieResourceParameters.PageSize);
      resultPaged.PagingStateCassandra = pagingStateCassandra;

      return resultPaged;

    }
    public async Task<IEnumerable<Movie>> GetMovies(MovieResourceParameters movieResourceParameters)
    {
      if (movieResourceParameters == null)
      {
        return await mapper.FetchAsync<Movie>();
      }
      if (string.IsNullOrWhiteSpace(movieResourceParameters.Genre)
      && string.IsNullOrWhiteSpace(movieResourceParameters.Title))
      {
        return await mapper.FetchAsync<Movie>();
      }      

      RowSet result = ExecuteCommand(ConstructSql(movieResourceParameters));
      return (IEnumerable<Movie>)result;
    }

    private string ConstructSql (MovieResourceParameters movieResourceParameters)
    {

      string sql = "SELECT id, title, category_id, media_type_id, discs, boxset, region_id FROM movie ";
      List<string> where = new List<string>();
      string orderBy = " ORDER BY title ";
      string ending = " ALLOW FILTERING; ";
      //var ps = session.Prepare("SELECT id, title, category_id, media_type_id, discs, boxset, region_id FROM movie WHERE category_id = ? AND title.c");

      if (!string.IsNullOrWhiteSpace(movieResourceParameters.Title))
      {
        var searchQuery = movieResourceParameters.Title.Trim();
        where.Add(string.Format(" title like '%{0}%'", searchQuery));
      }

      if (!string.IsNullOrWhiteSpace(movieResourceParameters.Genre))
      {
        var categoryId = (int)(MovieGenre)Enum.Parse(typeof(MovieGenre), movieResourceParameters.Genre.Trim(), true);
        where.Add(string.Format(" Category_id = {0}", categoryId));
      }
      return string.Concat(sql, String.Join(" AND ", where.ToArray()), orderBy, ending);
    }

    public async Task<Movie> GetMovie(int id)
    {

      RowSet result = ExecuteCommand(string.Format("SELECT id, title, category_id, media_type_id, discs, boxset, region_id FROM movie WHERE id = {0}",id));
      return (result as IEnumerable<Movie>).FirstOrDefault();
    }
    public async Task<int> AddMovie(Movie movie)
    {
      int newId = GetMovieRecordCount() + 1;

      movie.Id = newId;

      ExecuteCommand("INSERT INTO movie (id, title, category_id, media_type_id, discs, boxset, region_id) VALUES (" + movie.Id + ",'" + movie.Title + "'," + movie.Category_id + ",998, 6, TRUE, 4) IF NOT EXISTS;");
      
      return newId;
    }

    public void UpdateMovie(Movie movie)
    {
      ExecuteCommand(string.Format("UPDATE movie SET title = '{0}', category_id = {1} WHERE id = {2};", movie.Title, movie.Category_id, movie.Id));
    }

    public void RemoveMovie(Movie movie)
    {
      ExecuteCommand("DELETE FROM movie WHERE id = " + movie.Id + " IF EXISTS;");
    }
    private RowSet ExecuteCommand(string command)
    {
      _logger.Information(string.Format("Cassandra ExecuteCommand:{0}", command));
      session.Execute("use moviedb;");
      return session.Execute(command);

    }
    private int GetMovieRecordCount()
    {
      return (ExecuteCommand("SELECT id FROM movie") as IEnumerable<int>).Count();
    }
    private RowSet ExecuteCommandWithPageSize(string command, int pageSize, ref byte[] pagingState)
    {
      _logger.Information(string.Format("Cassandra ExecuteCommand:{0}", command));

      //Manual paging
      session.Execute("use moviedb;");
      var ps = session.Prepare(command);
      RowSet rs;
      //If you want to retrieve the next page of results only when you ask for it(for example, in a webpager), 
      //use the PagingState property in the RowSet to execute the following statement.

      // Disable automatic paging.
      if (pagingState == null)
      {
        //First page
        var statement = ps
          .Bind()
          .SetAutoPage(false)
          .SetPageSize(pageSize);
        rs = session.Execute(statement);
        pagingState = rs.PagingState;
      }
      else
      {
        // Later in time ...
        // Retrieve the following page of results.
        var statement = ps
           .Bind()
           .SetAutoPage(false)
           .SetPagingState(pagingState);
        rs = session.Execute(statement);
        pagingState = rs.PagingState;
      }
      return rs;
    }
  }
}
