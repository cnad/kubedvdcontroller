﻿using KubeMovieController.DbContexts;
using KubeMovieController.Entities;
using KubeMovieController.Helpers;
using KubeMovieController.ResourceParameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KubeMovieController.Services
{
  public class KubeMoviePostgresRepository : IKubeMovieRepository, IDisposable
  {
    private readonly MovieContext _context;
    private readonly IPropertyMappingService _propertyMappingService;
    public KubeMoviePostgresRepository(MovieContext context,
        IPropertyMappingService propertyMappingService)
    {
      _context = context ?? throw new ArgumentNullException(nameof(context));
      _propertyMappingService = propertyMappingService ??
               throw new ArgumentNullException(nameof(propertyMappingService));
    }
    public List<string> GetGenres()
    {
      return ((MovieGenre[])Enum.GetValues(typeof(MovieGenre))).Select(c => c.ToString()).ToList();
    }
    public Dictionary<int, string> GetGenresWithIds()
    {
      return ((MovieGenre[])Enum.GetValues(typeof(MovieGenre))).ToDictionary(v => (int)v, k => k.ToString());
    }
    public async Task<int> AddMovie(Movie movie)
    {
      if (movie == null)
      {
        throw new ArgumentNullException(nameof(movie));
      }

      _context.Movie.Add(movie);
      Save();
      return movie.Id;
    }
    public bool Save()
    {
      return (_context.SaveChanges() >= 0);
    }
    public async Task<IEnumerable<Movie>> GetMovies()
    {
      return GetMoviesIQueryable();
    }
    public async Task<PagedList<Movie>> GetMoviesPaged(MovieResourceParameters movieResourceParameters)
    {
      if (movieResourceParameters == null)
      {
        throw new ArgumentNullException(nameof(movieResourceParameters));
      }

      var collection = _context.Movie as IQueryable<Movie>;
      if (!string.IsNullOrWhiteSpace(movieResourceParameters.Genre))
      {
        var categoryId = (int)(MovieGenre)Enum.Parse(typeof(MovieGenre), movieResourceParameters.Genre.Trim(), true);
        collection = collection.Where(a => a.Category_id == categoryId);
      }

      if (!string.IsNullOrWhiteSpace(movieResourceParameters.Title))
      {

        var searchQuery = movieResourceParameters.Title.Trim();
        collection = collection.Where(a => a.Title.ToUpper().Contains(searchQuery.ToUpper()));
      }

      if (!string.IsNullOrWhiteSpace(movieResourceParameters.OrderBy))
      {
        // get property mapping dictionary
        var moviePropertyMappingDictionary =
            _propertyMappingService.GetPropertyMapping<Models.MovieDto, Movie>();

        collection = collection.ApplySort(movieResourceParameters.OrderBy,
            moviePropertyMappingDictionary);
      }

      return PagedList<Movie>.Create(collection,
          movieResourceParameters.PageNumber,
          movieResourceParameters.PageSize);
    }
    private IQueryable<Movie> GetMoviesIQueryable()
    {
      return _context.Movie;
    }
    public async Task<IEnumerable<Movie>> GetMovies(MovieResourceParameters movieResourceParameters)
    {
      if (movieResourceParameters == null)
      {
        return GetMoviesIQueryable();
      }
      if (string.IsNullOrWhiteSpace(movieResourceParameters.Genre)
     && string.IsNullOrWhiteSpace(movieResourceParameters.Title))
      {
        return GetMoviesIQueryable();
      }
      var collection = GetMoviesIQueryable();
      if (!string.IsNullOrWhiteSpace(movieResourceParameters.Genre))
      {

        var categoryId = (int)(MovieGenre)Enum.Parse(typeof(MovieGenre), movieResourceParameters.Genre.Trim(), true);
        collection = collection.Where(a => a.Category_id == categoryId);
      }
      if (!string.IsNullOrWhiteSpace(movieResourceParameters.Title))
      {

        var searchQuery = movieResourceParameters.Title.Trim();
        collection = collection.Where(a => a.Title.ToUpper().Contains(searchQuery.ToUpper()));
      }
      return collection.ToList();
    }


    public async Task<Movie> GetMovie(int id)
    {
      return _context.Movie.Where(x => x.Id == id).FirstOrDefault();
    }

    public void RemoveMovie(Movie movie)
    {
      if (movie == null)
      {
        throw new ArgumentNullException(nameof(movie));
      }

      _context.Movie.Remove(movie);
      Save();
    }

    public void UpdateMovie(Movie movie)
    {
      if (movie == null)
      {
        throw new ArgumentNullException(nameof(movie));
      }

      _context.Movie.Update(movie);
      Save();

    }

    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    protected virtual void Dispose(bool disposing)
    {
      if (disposing)
      {
        // dispose resources when needed
      }
    }
  }
}