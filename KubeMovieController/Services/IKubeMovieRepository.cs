﻿using KubeMovieController.Entities;
using KubeMovieController.Helpers;
using KubeMovieController.Models;
using KubeMovieController.ResourceParameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KubeMovieController.Services
{
  public interface IKubeMovieRepository
  {

    Task<IEnumerable<Movie>> GetMovies();
    Task<IEnumerable<Movie>> GetMovies(MovieResourceParameters movieParameters);
    Task<PagedList<Movie>> GetMoviesPaged(MovieResourceParameters movieResourceParameters);
    List<string> GetGenres();
    Dictionary<int, string> GetGenresWithIds();

    Task<Movie> GetMovie(int id);
    Task<int> AddMovie(Movie movie);
    void UpdateMovie(Movie movie);
    void RemoveMovie(Movie movie);



  }
}
