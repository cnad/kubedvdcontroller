using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KubeMovieController.DbContexts;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace KubeMovieController
{
  public class Program
  {
    public static void Main(string[] args)
    {
      //CreateHostBuilder(args).Build().Run();
      var host = CreateHostBuilder(args).Build();

      var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
      var isDevelopment = environment == Microsoft.AspNetCore.Hosting.EnvironmentName.Development;
      if (isDevelopment)
      {
        // migrate the database.  Best practice = in Main, using service scope
        using (var scope = host.Services.CreateScope())
        {
          try
          {
            var context = scope.ServiceProvider.GetService<SQLLiteInteractor>();

            // for demo purposes, delete the database & migrate on startup so 
            // we can start with a clean slate
            context.Database.EnsureDeleted();
            context.Database.Migrate();
          }
          catch (Exception ex)
          {
            var logger = scope.ServiceProvider.GetRequiredService<ILogger<Program>>();
            logger.LogError(ex, "An error occurred while migrating the database.");
          }

        }
      }

      // run the web app
      host.Run();
    }

    public static IHostBuilder CreateHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
            .ConfigureWebHostDefaults(webBuilder =>
            {
              webBuilder.UseStartup<Startup>();
            });
  }
}
