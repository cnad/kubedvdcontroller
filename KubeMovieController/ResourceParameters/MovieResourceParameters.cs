﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KubeMovieController.ResourceParameters
{
  public class MovieResourceParameters
  {
    public MovieResourceParameters()
    {
      PagingStateCassandra = null;
    }
    public string Genre { get; set; }
    public string Title { get; set; }

    const int maxPageSize = 200;

    public int PageNumber { get; set; } = 1;

    private int _pageSize = 5;
    public int PageSize
    {
      get => _pageSize;
      set => _pageSize = (value > maxPageSize) ? maxPageSize : value;
    }

    public string OrderBy { get; set; } = "Title";
    public string Fields { get; set; }

    public byte[] PagingStateCassandra { get; set; }
  }
}
