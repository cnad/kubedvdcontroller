using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using AutoMapper;
using KubeMovieController.Services;
using KubeMovieController.DbContexts;
using KubeMovieController.Models;
using KubeMovieController.DAO;
using System.IO;
using NLog;
using KubeMovieController.Logging;
using Microsoft.EntityFrameworkCore;
using KubeMovieController.Settings;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Formatters;

namespace KubeMovieController
{
  public class Startup
  {
    public Startup(IConfiguration configuration, IWebHostEnvironment env)
    {
      //var builder = new ConfigurationBuilder()
      //    .SetBasePath(env.ContentRootPath)
      //    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
      //    .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true)
      //    .AddEnvironmentVariables();

      //Configuration = builder.Build();
      Configuration = configuration;
      CurrentEnvironment = env;

      LogManager.LoadConfiguration(System.String.Concat(Directory.GetCurrentDirectory(), "/nlog.config"));
    }

    public IConfiguration Configuration { get; set; }
    public IWebHostEnvironment CurrentEnvironment { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      //var identitySettingsSection =
      //          Configuration.GetSection("PGIdentitySettings");
      //services.Configure<PGSettings>(identitySettingsSection);

      services.AddHttpCacheHeaders((expirationModelOptions) =>
      {
        expirationModelOptions.MaxAge = 60;
        expirationModelOptions.CacheLocation = Marvin.Cache.Headers.CacheLocation.Private;
      },
      (validationModelOptions) =>
      {
        validationModelOptions.MustRevalidate = true;
      });

      services.AddCors(options =>
      {
        options.AddPolicy("CorsPolicy",
            builder => builder.AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());
      });


      services.AddResponseCaching();


      //services.AddControllers();
      services.AddControllers(setupAction =>
      {
        setupAction.ReturnHttpNotAcceptable = true;
        setupAction.CacheProfiles.Add("240SecondsCacheProfile",
                                              new CacheProfile()
                                              {
                                                Duration = 240
                                              });

      }).AddNewtonsoftJson(setupAction =>
      {
        setupAction.SerializerSettings.ContractResolver =
           new CamelCasePropertyNamesContractResolver();
      })
       .AddXmlDataContractSerializerFormatters()
       .ConfigureApiBehaviorOptions(setupAction =>
       {
         setupAction.InvalidModelStateResponseFactory = context =>
         {
           var problemDetails = new ValidationProblemDetails(context.ModelState)
           {
             Type = "https://movies.com/modelvalidationproblem",
             Title = "One or more model validation errors occurred.",
             Status = StatusCodes.Status422UnprocessableEntity,
             Detail = "See the errors property for details.",
             Instance = context.HttpContext.Request.Path
           };

           problemDetails.Extensions.Add("traceId", context.HttpContext.TraceIdentifier);

           return new UnprocessableEntityObjectResult(problemDetails)
           {
             ContentTypes = { "application/problem+json" }
           };
         };
       });

      services.Configure<MvcOptions>(config =>
      {
        var newtonsoftJsonOutputFormatter = config.OutputFormatters
              .OfType<NewtonsoftJsonOutputFormatter>()?.FirstOrDefault();

        if (newtonsoftJsonOutputFormatter != null)
        {
          newtonsoftJsonOutputFormatter.SupportedMediaTypes.Add("application/vnd.marvin.hateoas+json");
        }
      });

      services.AddSingleton<ILog, LogNLog>();

      // register PropertyMappingService
      services.AddTransient<IPropertyMappingService, PropertyMappingService>();
      // register PropertyCheckerService
      services.AddTransient<IPropertyCheckerService, PropertyCheckerService>();


      //_logger.Information(string.Format(connectionString,
      services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
      if (CurrentEnvironment.IsDevelopment())
      {
        services.AddScoped<IKubeMovieRepository, KubeMovieSqlLiteRepository>();
        services.AddDbContext<SQLLiteInteractor>();

      }
      else if (CurrentEnvironment.IsProduction())
      {
        services.AddScoped<IKubeMovieRepository, KubeMoviePostgresRepository>();
        services.AddDbContext<MovieContext>();
      }
      //else if (CurrentEnvironment.IsEnvironment("ProductionCas"))
      //{
      //  services.AddScoped<ICassandraDAO, CassandraDAO>();
      //  services.AddScoped<IKubeMovieRepository, KubeMovieRepository>();
      //}

    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {

      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }
      else
      {
        app.UseExceptionHandler(appBuilder =>
        {
          appBuilder.Run(async context =>
          {
            context.Response.StatusCode = 500;
            await context.Response.WriteAsync("An unexpected fault happened. Try again later.");
          });
        });

      }

      app.UseCors("CorsPolicy");


      //order is important - before UseRouting
      //Marvin.Cache.Headers (4.0.0)
      //Marvin.Cache.Headers is middleware for ASP.NET Core which adds HttpCache headers to responses(Cache - Control, Expires, ETag, Last - Modified) 
      //and implements cache expiration & validation models.It can be used to ensure caches correctly cache responses and / or 
      //to implement concurrency for REST - based APIs using ETags.
      app.UseHttpCacheHeaders();

      app.UseRouting();

      app.UseAuthorization();

      app.UseEndpoints(endpoints =>
      {
        endpoints.MapControllers();
      });
    }
  }
}
