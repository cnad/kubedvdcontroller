﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cassandra;
using KubeMovieController.Logging;
using Microsoft.Extensions.Configuration;

namespace KubeMovieController.DAO
{
  public interface ICassandraDAO
  {
    ISession GetSession();
  }

  public class CassandraDAO : ICassandraDAO
  {
    private static Cluster Cluster;
    private static ISession Session;
    private ILog _logger;
    public CassandraDAO(ILog logger)
    {
      _logger = logger ??
     throw new ArgumentNullException(nameof(logger));
      SetCluster();
    }

    private void SetCluster()
    {
      if (Cluster == null)
      {
        Cluster = Connect();
      }
    }

    public ISession GetSession()
    {
      if (Cluster == null)
      {
        SetCluster();
        Session = Cluster.Connect();
      }
      else if (Session == null)
      {
        Session = Cluster.Connect();
      }

      return Session;
    }

    private Cluster Connect()
    {
      string user = "cassandra";
      string pwd = "cassandra";
      string ip = "cassandra-demo";
      int port = 9042;
      _logger.Information(string.Format("user = {0}={1}", user, Environment.GetEnvironmentVariable("CassandraUser")));
      _logger.Information(string.Format("pwd = {0}={1}", pwd, Environment.GetEnvironmentVariable("CassandraPassword")));
      _logger.Information(string.Format("ip = {0}={1}", ip, Environment.GetEnvironmentVariable("CassandraAddress")));
      _logger.Information(string.Format("port = {0}={1}", port, Environment.GetEnvironmentVariable("CassandraPort")));


      Cluster cluster = Cluster.Builder()
          .AddContactPoints(ip)
          .WithCredentials(user, pwd)
          .WithPort(port)
          .Build();

      return cluster;
    }

  }
}
