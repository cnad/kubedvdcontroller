﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cassandra;
using Cassandra.Mapping;
using KubeMovieController.Entities;
using KubeMovieController.Logging;

namespace KubeMovieController.DAO
{

    public interface IKubeMovieDAO
    {
        Task<IEnumerable<Movie>> getMovie();
        Task<IEnumerable<Movie>> addMovie(String title, int id);
        Task<IEnumerable<Movie>> updateMovie(String title, int id);
        Task<IEnumerable<Movie>> removeMovie(int id);

    }

    public class KubeMovieDAO : IKubeMovieDAO
    {
        protected readonly ISession session;
        protected readonly IMapper mapper;

        public KubeMovieDAO(ILog logger)
        {
            ICassandraDAO cassandraDAO = new CassandraDAO(logger);
            session = cassandraDAO.GetSession();
            mapper = new Mapper(session);
        }

        public async Task<IEnumerable<Movie>> getMovie()
        {
            return await mapper.FetchAsync<Movie>();
        }

        public async Task<IEnumerable<Movie>> addMovie(String title, int id)
        {
            session.Execute("use moviedb;"); 
            session.Execute("insert into movie (id, title, category_id, media_type_id, discs, boxset, region_id) values (" + id + ",'" + title + "',110,998, 6, TRUE, 4);");
            
            return await mapper.FetchAsync<Movie>();
        }

        public async Task<IEnumerable<Movie>> updateMovie(String title, int id)
        {
            session.Execute("use moviedb;"); 
            session.Execute("UPDATE movie SET title = '" + title + "' WHERE id = " + id + ";");
  
            return await mapper.FetchAsync<Movie>();
        }
        
        public async Task<IEnumerable<Movie>> removeMovie(int id)
        {
            session.Execute("use moviedb;"); 
            session.Execute("DELETE FROM movie WHERE id = " + id + " IF EXISTS;");
            return await mapper.FetchAsync<Movie>();
        }

    }
}

