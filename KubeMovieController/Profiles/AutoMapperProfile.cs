﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Expressions;
using KubeMovieController.Models;
using KubeMovieController.Helpers;
using System.Dynamic;

namespace KubeMovieController.Profiles
{
  public class AutoMapperProfile : Profile
  {
    public AutoMapperProfile()
    {
      CreateMap<Entities.Movie, Models.MovieDto>()
          .ForMember(
              dest => dest.CategoryId,
              opt => opt.MapFrom(src => src.Category_id))
           .ForMember(
              dest => dest.ExampleProperty,
              opt => opt.MapFrom(src => string.Concat(src.Id, ":", src.Title)));


      CreateMap<Models.MovieForCreationDto, Entities.Movie>()
        .ForMember(
              dest => dest.Category_id, opt => opt.MapFrom(src => (MovieGenre)System.Enum.Parse(typeof(MovieGenre), src.Genre,true))); 
      CreateMap<Models.MovieForUpdateDto, Entities.Movie>()
           .ForMember(
              dest => dest.Category_id, opt => opt.MapFrom(src => (MovieGenre)System.Enum.Parse(typeof(MovieGenre), src.Genre,true)));
      CreateMap<Entities.Movie, Models.MovieForUpdateDto>()
           .ForMember(
              dest => dest.Genre, opt => opt.MapFrom(src => Enum.GetName(typeof(MovieGenre), src.Category_id)));

    }

  }
}
