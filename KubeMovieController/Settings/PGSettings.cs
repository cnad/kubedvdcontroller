﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KubeMovieController.Settings
{
  public class PGSettings
  {
    public string PGPassword { get; set; }
    public string PGUser { get; set; }
    public string PGHost { get; set; }
    public int PGPort { get; set; }
    public string PGDatabase { get; set; }
    public int PGVersionMajor { get; set; }
    public int PGVersionMinor { get; set; }

    public string PGConnectionString { get; set; }
  }
}
