﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using Marvin.Cache.Headers;
using KubeMovieController.Services;
using KubeMovieController.Logging;
using KubeMovieController.ResourceParameters;
using KubeMovieController.Models;
using KubeMovieController.Helpers;
using KubeMovieController.ActionConstraints;

namespace KubeMovieController.Controllers
{
  //[EnableCors("CorsPolicy")]
  [Route("api/[controller]/[action]")]
  [ApiController]
  [HttpCacheExpiration(CacheLocation = CacheLocation.Public)]
  [HttpCacheValidation(MustRevalidate = true)]
  public class MovieController : ControllerBase
  {
    private readonly IKubeMovieRepository _kubeMovieRepository;
    private readonly IMapper _mapper;
    private readonly IPropertyMappingService _propertyMappingService;
    private readonly IPropertyCheckerService _propertyCheckerService;
    private ILog _logger;
    public MovieController(IKubeMovieRepository kubeMovieRepository, IMapper mapper, IPropertyMappingService propertyMappingService, 
      IPropertyCheckerService propertyCheckerService, ILog logger)
    {
      _kubeMovieRepository = kubeMovieRepository ??
          throw new ArgumentNullException(nameof(kubeMovieRepository));

      _mapper = mapper ??
          throw new ArgumentNullException(nameof(mapper));
      _propertyMappingService = propertyMappingService ??
            throw new ArgumentNullException(nameof(propertyMappingService));
      _propertyCheckerService = propertyCheckerService ??
            throw new ArgumentNullException(nameof(propertyCheckerService));
      _logger = logger ??
      throw new ArgumentNullException(nameof(logger));

    }

    // GET: api/Movie/GetGenres    
    [HttpGet(Name = "GetEnv")]
    [ActionName("GetEnv")]
    public ActionResult GetEnv([FromQuery] string section)
    {
      var value = System.Environment.GetEnvironmentVariable(section);

      Dictionary<string, string> env = new Dictionary<string, string>() { 
        { section.ToLower(), string.IsNullOrEmpty(value) ? "" : value } 
      };
      string data = Newtonsoft.Json.JsonConvert.SerializeObject(env, Newtonsoft.Json.Formatting.Indented);
      return Ok(Newtonsoft.Json.JsonConvert.DeserializeObject(data));
    }

    // GET: api/Movie/GetGenres    
    [HttpGet(Name = "GetGenres")]
    [ActionName("GetGenres")]
    public ActionResult GetGenres()
    {
      return Ok(_kubeMovieRepository.GetGenres());
    }

    [HttpGet(Name = "GetGenresWithIds")]
    [ActionName("GetGenresWithIds")]
    public ActionResult GetGenresWithIds()
    {
      return Ok(_kubeMovieRepository.GetGenresWithIds());
    }
    // GET: api/Movie
    [HttpGet(Name = "GetMoviesLinks")]
    [ActionName("GetMoviesLinks")]
    public ActionResult GetMoviesLinks([FromQuery] MovieResourceParameters movieResourceParameters)
    {

      if (!_propertyMappingService.ValidMappingExistsFor<MovieDto, Entities.Movie>
    (movieResourceParameters.OrderBy))
      {
        return BadRequest();
      }
      if (!_propertyCheckerService.TypeHasProperties<MovieDto>
              (movieResourceParameters.Fields))
      {
        return BadRequest();
      }
      var moviesFromRepo = _kubeMovieRepository.GetMoviesPaged(movieResourceParameters).Result;

      var paginationMetadata = new
      {
        totalCount = moviesFromRepo.TotalCount,
        pageSize = moviesFromRepo.PageSize,
        currentPage = moviesFromRepo.CurrentPage,
        totalPages = moviesFromRepo.TotalPages
      };

      Response.Headers.Add("X-Pagination",
          JsonSerializer.Serialize(paginationMetadata));

      var links = CreateLinksForMovies(movieResourceParameters,
          moviesFromRepo.HasNext,
          moviesFromRepo.HasPrevious);

      var shapedMovies = _mapper.Map<IEnumerable<MovieDto>>(moviesFromRepo)
                         .ShapeData(movieResourceParameters.Fields);

      var shapedMoviesWithLinks = shapedMovies.Select(movie =>
      {
        var movieAsDictionary = movie as IDictionary<string, object>;
        var movieLinks = CreateLinksForMovie((int)movieAsDictionary["Id"], null);
        movieAsDictionary.Add("links", movieLinks);
        return movieAsDictionary;
      });

      var linkedCollectionResource = new
      {
        value = shapedMoviesWithLinks,
        links
      };

      return Ok(linkedCollectionResource);

    }
    // GET: api/Movie
    [HttpGet(Name = "GetMovies")]
    [ActionName("GetMovies")]
    public ActionResult GetMovies([FromQuery] MovieResourceParameters movieResourceParameters)
    {

      if (!_propertyMappingService.ValidMappingExistsFor<MovieDto, Entities.Movie>
    (movieResourceParameters.OrderBy))
      {
        return BadRequest();
      }
      if (!_propertyCheckerService.TypeHasProperties<MovieDto>
              (movieResourceParameters.Fields))
      {
        return BadRequest();
      }
      var moviesFromRepo = _kubeMovieRepository.GetMoviesPaged(movieResourceParameters).Result;

      var previousPageLink = moviesFromRepo.HasPrevious ?
          CreateMoviesResourceUri(movieResourceParameters,
          ResourceUriType.PreviousPage) : null;

      var nextPageLink = moviesFromRepo.HasNext ?
          CreateMoviesResourceUri(movieResourceParameters,
          ResourceUriType.NextPage) : null;
      
      //need to supply pagingState for Cassandra (in object or in header)
      byte[]  pagingState = new byte[] { 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20 };
      var paginationMetadata = new
      {
        totalCount = moviesFromRepo.TotalCount,
        pageSize = moviesFromRepo.PageSize,
        currentPage = moviesFromRepo.CurrentPage,
        totalPages = moviesFromRepo.TotalPages,
        previousPageLink,
        nextPageLink,
        pagingState = pagingState
      };

      Response.Headers.Add("X-Pagination",
          JsonSerializer.Serialize(paginationMetadata));

      return Ok(_mapper.Map<IEnumerable<MovieDto>>(moviesFromRepo)
          .ShapeData(movieResourceParameters.Fields));
    }

    // GET: api/Movie/5    
    [HttpGet("{id}", Name = "GetMovie")]
    [ActionName("GetMovie")]
    [HttpCacheExpiration(CacheLocation = CacheLocation.Public, MaxAge = 1000)]
    [HttpCacheValidation(MustRevalidate = false)]
    public ActionResult GetMovie(int id, string fields)
    {
      if (!_propertyCheckerService.TypeHasProperties<MovieDto>
        (fields))
      {
        return BadRequest();
      }

      var movieFromRepo = _kubeMovieRepository.GetMovie(id).Result;

      //it can be 0 or non existing id
      if (movieFromRepo == null)
      {
        return NotFound("Invalid Id");
      }

      return Ok(_mapper.Map<MovieDto>(movieFromRepo).ShapeData(fields));
    }

    // POST: api/Movie
    [HttpPost]
    [ActionName("CreateMovie")]
    [RequestHeaderMatchesMediaType("Content-Type",
            "application/json",
            "application/vnd.marvin.movieforcreation+json")]
    [Consumes("application/json",
            "application/vnd.marvin.movieforcreation+json")]
    public ActionResult<MovieDto> CreateMovie(MovieForCreationDto movie)
    {
      if (!TryValidateModel(movie))
      {
        return ValidationProblem(ModelState);
      }
      
      var movieEntity = _mapper.Map<Entities.Movie>(movie);
      int returnid =0; 
      returnid = _kubeMovieRepository.AddMovie(movieEntity).Result;

      var movieToReturn = _mapper.Map<MovieDto>(movieEntity);

      return CreatedAtRoute("GetMovie",
          new { id = returnid},
          movieToReturn);
    }

    // PUT: api/Movie/5
    [HttpPut("{id}", Name = "UpdateMovie")]
    [ActionName("UpdateMovie")]
    public IActionResult UpdateMovie(int id, MovieForUpdateDto movie)
    {
      if (!TryValidateModel(movie))
      {
        return ValidationProblem(ModelState);
      }

      var movieFromRepo = _kubeMovieRepository.GetMovie(id).Result;

      if (movieFromRepo == null)
      {
        return NotFound();
      }
      movieFromRepo.Title = movie.Title;
      movieFromRepo.Category_id = (int)(MovieGenre)System.Enum.Parse(typeof(MovieGenre), movie.Genre, true);

      _kubeMovieRepository.UpdateMovie(movieFromRepo);

      return NoContent();


    }

    // DELETE: api/ApiWithActions/5
    [HttpDelete("{id}", Name = "DeleteMovie")]
    [ActionName("DeleteMovie")]
    public ActionResult DeleteMovie(int id)
    {
      var movieFromRepo =  _kubeMovieRepository.GetMovie(id).Result;

      if (movieFromRepo == null)
      {
        return NotFound();
      }

      _kubeMovieRepository.RemoveMovie(movieFromRepo);      

      return NoContent();
    }

    private string CreateMoviesResourceUri(MovieResourceParameters movieResourceParameters, ResourceUriType type)
    {
      switch (type)
      {
        case ResourceUriType.PreviousPage:
          return Url.Link("GetMovies",
            new
            {
              fields = movieResourceParameters.Fields,
              orderBy = movieResourceParameters.OrderBy,
              pageNumber = movieResourceParameters.PageNumber - 1,
              pageSize = movieResourceParameters.PageSize,
              genre = movieResourceParameters.Genre,
              title = movieResourceParameters.Title
            });
        case ResourceUriType.NextPage:
          return Url.Link("GetMovies",
            new
            {
              fields = movieResourceParameters.Fields,
              orderBy = movieResourceParameters.OrderBy,
              pageNumber = movieResourceParameters.PageNumber + 1,
              pageSize = movieResourceParameters.PageSize,
              genre = movieResourceParameters.Genre,
              title = movieResourceParameters.Title
            });

        default:
          return Url.Link("GetMovies",
          new
          {
            fields = movieResourceParameters.Fields,

            orderBy = movieResourceParameters.OrderBy,
            pageNumber = movieResourceParameters.PageNumber,
            pageSize = movieResourceParameters.PageSize,
            genre = movieResourceParameters.Genre,
            title = movieResourceParameters.Title
          });
      }

    }
    private IEnumerable<LinkDto> CreateLinksForMovies(MovieResourceParameters movieResourceParameters, bool hasNext, bool hasPrevious)    
    {
      var links = new List<LinkDto>();

      // self 
      links.Add(
         new LinkDto(CreateMoviesResourceUri(
             movieResourceParameters, ResourceUriType.Current)
         , "self", "GET"));

      if (hasNext)
      {
        links.Add(
          new LinkDto(CreateMoviesResourceUri(
              movieResourceParameters, ResourceUriType.NextPage),
          "nextPage", "GET"));
      }

      if (hasPrevious)
      {
        links.Add(
            new LinkDto(CreateMoviesResourceUri(
                movieResourceParameters, ResourceUriType.PreviousPage),
            "previousPage", "GET"));
      }

      return links;
    }
    private IEnumerable<LinkDto> CreateLinksForMovie(int movieId, string fields)
    {

      var links = new List<LinkDto>();

      if (string.IsNullOrWhiteSpace(fields))
      {
        links.Add(
          new LinkDto(Url.Link("GetMovie", new { id = movieId }),
          "self",
          "GET"));
      }
      else
      {
        links.Add(
          new LinkDto(Url.Link("GetMovie", new { id = movieId, fields }),
          "self",
          "GET"));
      }

      links.Add(
         new LinkDto(Url.Link("DeleteMovie", new { id = movieId }),
         "delete_movie",
         "DELETE"));

      links.Add(
          new LinkDto(Url.Link("CreateMovie", new { id = movieId }),
          "create_movie",
          "POST"));

      links.Add(
         new LinkDto(Url.Link("UpdateMovie", new { id = movieId }),
         "update_movie",
         "PUT"));

      return links;
    }

  }
}
