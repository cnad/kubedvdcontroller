﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Cassandra.Mapping.Attributes;

namespace KubeMovieController.Entities
{
  [Table("moviedb.movie")] //selects keyspace 
  public class Movie
  {
    public Movie()
    {

    }
    public Movie(int id, string title, int category)
    {
      Id = id;
      Title = title;
      Category_id = category;
    }
    [Required]
    [Range(1, int.MaxValue, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
    public int Id { get; set; }

    [Required]
    [MaxLength(100)]
    public string Title { get; set; }
    public int Category_id { get; set; }
    public int Media_type_id { get; set; }
    public int Discs { get; set; }
    public int Region_id { get; set; }
    
    //[System.ComponentModel.DataAnnotations.Schema.ForeignKey("category_id")]
    //public virtual Genre Genre { get; set; }
  }
}
