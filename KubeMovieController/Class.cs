﻿using Cassandra;
using KubeDvdController.Entities;
using KubeDvdController.Helpers;
using KubeDvdController.Logging;
using KubeDvdController.ResourceParameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KubeDvdController
{
  public  class Class
  {
    ILog _logger;
    public  Class (ILog logger)
    {
      _logger = logger;
    }
    public PagedList<Dvd> GetDvdsPaged(DvdResourceParameters dvdResourceParameters)
    {
      if (dvdResourceParameters == null)
      {
        throw new ArgumentNullException(nameof(dvdResourceParameters));
      }

      string sql = ConstructSql(dvdResourceParameters);
      byte[] pagingStateCassandra = dvdResourceParameters.PagingStateCassandra;
      IEnumerable<Dvd> result = ExecuteCommandWithPageSize(sql, dvdResourceParameters.PageSize, ref pagingStateCassandra);

      int count = GetDvdRecordCount();
      PagedList<Dvd> resultPaged = new PagedList<Dvd>((result as IEnumerable<Dvd>).ToList(), count, dvdResourceParameters.PageNumber, dvdResourceParameters.PageSize);
      resultPaged.PagingStateCassandra = pagingStateCassandra;

      return resultPaged;

    }
    private int GetDvdRecordCount()
    {
      return 12;
    }
    private IEnumerable<Dvd> ExecuteCommandWithPageSize(string command, int pageSize, ref byte[] pagingState)
    {
      _logger.Information(string.Format("Cassandra ExecuteCommand:{0}", command));

      RowSet rs;
      List<Dvd> dvds = new List<Dvd> { 
        new Dvd(1,"aaa",100),
        new Dvd(2,"bbb",101)
      };

      pagingState = new byte[] { 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20 };
      return (dvds as IEnumerable<Dvd>);
    }
    private string ConstructSql(DvdResourceParameters dvdResourceParameters)
    {

      string sql = "SELECT id, title, category_id, media_type_id, discs, boxset, region_id FROM dvd ";
      List<string> where = new List<string>();
      string orderBy = " ORDER BY title ";
      string ending = " ALLOW FILTERING; ";

      if (!string.IsNullOrWhiteSpace(dvdResourceParameters.Title))
      {
        var searchQuery = dvdResourceParameters.Title.Trim();
        where.Add(string.Format(" title like '%{0}%'", searchQuery)) ;
      }

      if (!string.IsNullOrWhiteSpace(dvdResourceParameters.Genre))
      {
        var categoryId = (int)(MovieGenre)Enum.Parse(typeof(MovieGenre), dvdResourceParameters.Genre.Trim(), true);
        where.Add(string.Format(" Category_id = {0}", categoryId));
      }
      return string.Concat(sql, String.Join(" AND ", where.ToArray()), orderBy, ending);
    }
  }
}
