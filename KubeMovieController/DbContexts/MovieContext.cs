﻿using KubeMovieController.Settings;
using KubeMovieController.Entities;
using KubeMovieController.Logging;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KubeMovieController.DbContexts
{

  public class MovieContext : DbContext
  {
    private ILog _logger;
    //private PGSettings _settings;
    //public MovieContext(DbContextOptions<MovieContext> options, ILog logger, IOptions<PGSettings> appIdentitySettingsAccessor)
    public MovieContext(DbContextOptions<MovieContext> options, ILog logger)
        : base(options)
    {
      _logger = logger ??
throw new ArgumentNullException(nameof(logger));
      //if (appIdentitySettingsAccessor != null)
      //  _settings = appIdentitySettingsAccessor.Value;

    }
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
      //var connectionString = @"User ID=postgres;Password=Password1;Host=localhost;Port=5432;Database=moviedb;Pooling=true;SSL Mode=Require;Trust Server Certificate=true";
      var connectionString = ConstructPGConnectionString();
      int verMajor = 9;
      int verMinor = 2;
      //if (_settings != null)
      //{
      //  verMajor= _settings.PGVersionMajor;
      //  verMinor = _settings.PGVersionMinor;
      //}

      //optionsBuilder.UseNpgsql(connectionString, options => options.SetPostgresVersion(new Version(verMajor, verMinor))).UseSnakeCaseNamingConvention();
      optionsBuilder.UseNpgsql(connectionString).UseSnakeCaseNamingConvention();
    }

    public DbSet<Movie> Movie { get; set; }
    //public DbSet<Genre> Genre { get; set; }

    private string ConstructPGConnectionString()
    {
      //var connectionString = @"User ID={0};Password={1};Host={2};Port={3};Database={4};Pooling=true;";
      //if (_settings.PGHost != "localhost")
      //  connectionString +="SSL Mode=Require;Trust Server Certificate=true";
      //_logger.Information("IN ConstructPGConnectionString:");
      //return string.Format(connectionString,
      //  _settings.PGUser,
      //  _settings.PGPassword,
      //  _settings.PGHost,
      //  _settings.PGPort,
      //  _settings.PGDatabase
      //  );
      //_logger.Information(a);
      //return string.Format(connectionString, "simonr", "password", "acid-minimal-cluster", 5432, "moviedb");
      
      return System.Environment.GetEnvironmentVariable("PGConnectionString");

    }
  }
}

