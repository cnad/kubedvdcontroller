﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using KubeMovieController.Entities;

namespace KubeMovieController.DbContexts
{
  public class SQLLiteInteractor : DbContext
  {
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {

      optionsBuilder.UseSqlite(@"Data Source = Movie.db;");
    }
    public DbSet<Movie> Movie { get; set; }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      modelBuilder.Entity<Movie>().HasData(
      new Movie(1, "Movie Title 1", 101),
      new Movie(2, "Movie Title 2", 102),
      new Movie(3, "Movie Title 3", 104),
      new Movie(4, "Movie Title 4", 104),
      new Movie(5, "Movie Title 5", 105),
      new Movie(6, "Movie Title 6", 106),
      new Movie(7, "Movie Title 7", 101),
      new Movie(8, "Movie Title 12", 102)
      );
    }
  }
}
