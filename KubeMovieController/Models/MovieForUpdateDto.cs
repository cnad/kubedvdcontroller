﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace KubeMovieController.Models
{
  public class MovieForUpdateDto : MovieForManipulationDto
  {
    //some specific validation in here
    [Required(ErrorMessage = "You should fill out a genre.")]
    public string Genre { get; set; }
  }
}
