﻿using KubeMovieController.Helpers;
using System;
using System.ComponentModel.DataAnnotations;
namespace KubeMovieController.Models
{

  public class MovieDto
  {
    private int _categoryId;
    string _genre = MovieGenre.Unknown.ToString();
    [Required(ErrorMessage = "Please Enter a valid Id. \n")]
    [RegularExpression("^[0-9]$", ErrorMessage = "Please Enter a valid Id. \n")]
    public int Id { get; set; }
    public string Title { get; set; }

    public string Genre { get; set; }

    public int CategoryId {
      get { return _categoryId; }
      set
      {
        _categoryId = value;
        Genre = Enum.GetName(typeof(MovieGenre), value).ToString();
      }
    }
    public string ExampleProperty { get; set; }
  }
}
