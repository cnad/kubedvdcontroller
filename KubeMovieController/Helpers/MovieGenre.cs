﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KubeMovieController.Helpers
{
  public enum MovieGenre
  {
    Unknown = 0,
    SuperHeroMarvel = 100,
    SuperHeroDC = 101,
    SuperHero = 102,
    Action = 104,
    Comedy = 105,
    Horror = 106,
    Thriller = 107,
    Drama = 108,
    Musical = 109,
    ScienceFiction = 110,
    Animation = 111,
    Anime = 112,
    JamesBond = 113


  }
}
