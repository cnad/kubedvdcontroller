﻿using AutoMapper;
using KubeMovieController.Controllers;
using KubeMovieController.DbContexts;
using KubeMovieController.Entities;
using KubeMovieController.Helpers;
using KubeMovieController.Logging;
using KubeMovieController.Models;
using KubeMovieController.Profiles;
using KubeMovieController.ResourceParameters;
using KubeMovieController.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Moq;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Common;
using System.Linq;
using Xunit;

namespace KubeDvdController.Test
{
  public class TestMovieContext
  {
    public MovieContext ContextToTest { get; private set; }
    [Fact]
    public void a()
    {

      var connectionString = @"User ID=postgres;Password=Password1;Host=localhost;Port=5432;Database=movie;Pooling=true;";
      var optionsBuilder = new DbContextOptionsBuilder<MovieContext>();
      optionsBuilder.UseNpgsql(connectionString, options => options.SetPostgresVersion(new Version(9, 2))).UseSnakeCaseNamingConvention();
      MovieContext context = new MovieContext(optionsBuilder.Options);
      //MovieContext context = new MovieContext();
      var aa = context.Movie.ToList().Count;
      var b = 2;
    }
  }
}
