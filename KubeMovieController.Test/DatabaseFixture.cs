﻿using AutoMapper;
using KubeMovieController.Controllers;
using KubeMovieController.DbContexts;
using KubeMovieController.Entities;
using KubeMovieController.Models;
using KubeMovieController.Profiles;
using KubeMovieController.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using Xunit;

namespace KubeMovieController.Test
{
  public class DatabaseFixture : IDisposable
  {
    public SQLLiteInteractor ContextToTest { get; private set; }
    public DatabaseFixture()
    {
      // Do "global" initialization here; Called before every test method.
      // SQLLiteInteractor
      //Migrate Populates DB with the data, which was supplied in KubeMovieController.DbContexts.SQLLiteInteractor.OnModelCreating
      ContextToTest = new SQLLiteInteractor();
      ContextToTest.Database.EnsureDeleted();
      ContextToTest.Database.Migrate();
      ContextToTest.Dispose();


      //If we want to use own set of data - we need to clear the table, reseed the Ids and seed with different set of data
      AdjustTestData();
      // ... initContextToTest.Movieialize data in the test database ...
    }

    public void Dispose()
    {
      // ... clean up test data from the database ...
      // Do "global" teardown here; Called after every test method.
      ContextToTest.Database.EnsureDeleted();
    }

    private void AdjustTestData()
    {
      //If we want to use own set of data - we need to clear the table, reseed the Ids and seed with different set of data
      ClearTestData();
      SeedMovies();
    }
    private void ClearTestData()
    {
      string path = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "Movie.db"); ;
      string cs = $"URI=file:{path}";
      SQLiteConnection con = new SQLiteConnection(cs);
      con.Open();

      using var cmd = new SQLiteCommand(con);

      cmd.CommandText = "DELETE FROM movie";
      cmd.ExecuteNonQuery();

      cmd.CommandText = "DELETE FROM sqlite_sequence WHERE name = 'Movie'";
      cmd.ExecuteNonQuery();

      con.Close();
    }
    private void SeedMovies()
    {
      ContextToTest = new SQLLiteInteractor();
      List<Movie> data = new List<Movie>(){
      new Movie(1, "DVD Title 1", 101),
      new Movie(2, "DVD Title 2", 102),
      new Movie(3, "DVD Title 3", 104),
      new Movie(4, "DVD Title 4", 101),
      new Movie(5, "DVD Title 5", 105),
      new Movie(6, "DVD Title 6", 106),
      new Movie(7, "DVD Title 7", 101),
      new Movie(8, "DVD Title 11", 102)
      };
      ContextToTest.Movie.AddRange(data);
      ContextToTest.SaveChanges();
    }
  }
}