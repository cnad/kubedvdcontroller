﻿using AutoMapper;
using KubeMovieController.Controllers;
using KubeMovieController.DbContexts;
using KubeMovieController.Entities;
using KubeMovieController.Helpers;
using KubeMovieController.Logging;
using KubeMovieController.Models;
using KubeMovieController.Profiles;
using KubeMovieController.ResourceParameters;
using KubeMovieController.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Moq;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Common;
using System.Linq;
using Xunit;

namespace KubeMovieController.Test
{
  public class TestMovieController : IClassFixture<DatabaseFixture>
  {
   
    private SQLLiteInteractor _context;
    private IKubeMovieRepository _kubeMovieRepository;
    private IMapper _mapper;
    private MovieController _movieController;
    private IPropertyMappingService _propertyMappingService;
    private MovieResourceParameters _movieResourceParameters;
    private IPropertyCheckerService _propertyCheckerService;
    public  TestMovieController(DatabaseFixture fixture)
    {     
      //Configure AutoMapper 
      var config = new MapperConfiguration(opts =>
      {
        opts.AddProfile<AutoMapperProfile>();

      });
       

      _mapper = config.CreateMapper(); // Use this mapper to instantiate your class

      _propertyMappingService = new PropertyMappingService();
      _propertyCheckerService = new PropertyCheckerService();
      // SQLLiteInteractor
      _context = fixture.ContextToTest;
      List<Movie> movies0 = _context.Movie.ToList();

      ILog logger = new LogNLog();

      //KubeMovieSqlLiteRepository
      _kubeMovieRepository = new KubeMovieSqlLiteRepository(_context, _propertyMappingService);

      //MovieController
      _movieController = new MovieController(_kubeMovieRepository, _mapper, _propertyMappingService, _propertyCheckerService, logger);
      Mock<IUrlHelper> urlHelper = new Mock<IUrlHelper>();
      urlHelper.Setup(x => x.Link(It.IsAny<string>(), It.IsAny<object>())).Returns("http://localhost");
      _movieController.Url = urlHelper.Object;

      // Ensure the controller can add response headers
      _movieController.ControllerContext = new ControllerContext();
      _movieController.ControllerContext.HttpContext = new DefaultHttpContext();
      _movieController.ControllerContext.HttpContext.Response.Headers.Clear();

      _movieResourceParameters = new MovieResourceParameters
      {
        PageNumber = 1,
        PageSize = 100
      };
    }
    #region Getgenres
    [Fact]
    public void Get_Genres_WhenCalled_ReturnsOkResultWithItems()
    {
      // Arrange
      int count = ((MovieGenre[])Enum.GetValues(typeof(MovieGenre))).Select(c => c.ToString()).ToList().Count;
      // Act
      var okResult = _movieController.GetGenres();

      // Assert
      Assert.IsType<OkObjectResult>(okResult);
      Assert.Equal(count, ((okResult as OkObjectResult).Value as List<string>).Count);
    }
    [Fact]
    public void Get_GenresEWithIds_WhenCalled_ReturnsOkResultWithItems()
    {
      // Arrange
      int count = ((MovieGenre[])Enum.GetValues(typeof(MovieGenre))).Select(c => c.ToString()).ToList().Count;
      // Act
      var okResult = _movieController.GetGenresWithIds();

      // Assert
      Assert.IsType<OkObjectResult>(okResult);
      Assert.Equal(count, ((okResult as OkObjectResult).Value as Dictionary<int, string>).Count);
    }
    #endregion
    #region GetMovies
    [Fact]
    public void Get_Movies_WhenCalled_ReturnsOkResult()
    {
      // Act
      var okResult = _movieController.GetMovies(_movieResourceParameters);

      // Assert
      Assert.IsType<OkObjectResult>(okResult);
    }
    [Fact]
    public void Get_Movies_WhenCalled_ReturnsAllItems()
    {
      // Arrange
      List<Movie> movies = _context.Movie.ToList();

      // Act
      var okResult = _movieController.GetMovies(_movieResourceParameters) as OkObjectResult;

      // Assert
      var a = okResult.Value;
      var items = Assert.IsType<List<System.Dynamic.ExpandoObject>>(okResult.Value);
      Assert.Equal(movies.Count, items.Count);
    }
    [Fact]
    public void Get_Movies_WhenCalledWithEmptyParameters_ReturnsAllItems()
    {
      // Arrange
      List<Movie> movies = _context.Movie.ToList();


      // Act
      var okResult = _movieController.GetMovies(_movieResourceParameters) as OkObjectResult;

      // Assert
      var items = Assert.IsType<List<System.Dynamic.ExpandoObject>>(okResult.Value);
      Assert.Equal(movies.Count, items.Count);
    }
    
    [Fact]
    public void Get_Movies_PageNumberPageSizeOrderByGenreTitle_ReturnsAllItems()
    {
      // Arrange      
      List<Movie> movies = _context.Movie.ToList();
      MovieGenre genre = MovieGenre.SuperHero;
      int categoryId = (int)(MovieGenre)Enum.Parse(typeof(MovieGenre), genre.ToString(), true);
      int count = movies.Where(x => x.Title.Contains("2") && x.Category_id == categoryId).Select(x => x).ToList().Count;
      int pageSize = 10;
      int pageNumber = 1;
      MovieResourceParameters movieParameters = new MovieResourceParameters
      {
        PageNumber = pageNumber,
        PageSize = pageSize,
        OrderBy = "title",
        Genre = genre.ToString(),
        Title = "2"
      };
      // Act
      var okResult = _movieController.GetMovies(movieParameters) as OkObjectResult;

      // Assert
      var items = Assert.IsType<List<System.Dynamic.ExpandoObject>>(okResult.Value);
      Assert.Equal(count, items.Count);
    }
    //api/Movie?fields=FirstName

    [Fact]
    public void Get_Movies_WrongFields_ShouldFail()
    {
      // Arrange      
      List<Movie> movies = _context.Movie.ToList();
      MovieResourceParameters movieParameters = new MovieResourceParameters
      {        
        Fields = "FirstName"
      };
      // Act
      var badRequest = _movieController.GetMovies(movieParameters);      

      // Assert
      Assert.IsType<BadRequestResult>(badRequest);
    }

    [Theory]
    [InlineData(1)]
    [InlineData(2)]
    public void Get_Movies_PageNumberPageSize_ReturnsAllItems(int pageNumber)
    {
      // Arrange      
      List<Movie> movies = _context.Movie.ToList();
      int pageSize = 5;
      MovieResourceParameters movieParameters = new MovieResourceParameters
      {
        PageNumber = pageNumber,
        PageSize = pageSize
      };
      // Act
      var okResult = _movieController.GetMovies(movieParameters) as OkObjectResult;

      // Assert
      var items = Assert.IsType<List<System.Dynamic.ExpandoObject>>(okResult.Value);
      Assert.Equal(movies.Take(pageNumber * pageSize).ToList().Count - (pageSize * (pageNumber - 1)), items.Count);
    }
    [Fact]
    public void Get_Movies_WhenCalledWithGenre_ReturnsAllItems()
    {
      // Arrange
      List<Movie> movies = _context.Movie.ToList();
      string genre = "superherodc";
      MovieResourceParameters movieParameters = new MovieResourceParameters
      {
        Genre = genre
      };
      var categoryId = (int)(MovieGenre)Enum.Parse(typeof(MovieGenre), genre, true);
      int count = movies.Where(x => x.Category_id == categoryId).Select(x => x).ToList().Count;

      // Act
      var okResult = _movieController.GetMovies(movieParameters) as OkObjectResult;

      // Assert
      var items = Assert.IsType<List<System.Dynamic.ExpandoObject>>(okResult.Value);
      Assert.Equal(count, items.Count);
    }
    [Fact]
    public void Get_Movies_WhenCalledWithTitle_ReturnsAllItems()
    {
      // Arrange
      List<Movie> movies = _context.Movie.ToList();      
      string title = "1";
      MovieResourceParameters movieParameters = new MovieResourceParameters
      {
        Title = title
      };
      int count = movies.Where(x => 
      x.Title.Contains(title)
      ).Select(x => x).ToList().Count;

      // Act
      var okResult = _movieController.GetMovies(movieParameters) as OkObjectResult;

      // Assert
      var items = Assert.IsType<List<System.Dynamic.ExpandoObject>>(okResult.Value);
      Assert.Equal(count, items.Count);
    }
    [Fact]
    public void Get_Movies_WhenCalledWithGenreAndTitle_ReturnsAllItems()
    {
      // Arrange
      List<Movie> movies = _context.Movie.ToList();
      string genre = "superherodc";
      string title = "1";
      MovieResourceParameters movieParameters = new MovieResourceParameters
      {
        Genre = genre,
        Title = title
      };
      var categoryId = (int)(MovieGenre)Enum.Parse(typeof(MovieGenre), genre, true);
      int count = movies.Where(x => x.Category_id == categoryId &&
      x.Title.Contains(title)
      ).Select(x => x).ToList().Count;

      // Act
      var okResult = _movieController.GetMovies(movieParameters) as OkObjectResult;

      // Assert
      var items = Assert.IsType<List<System.Dynamic.ExpandoObject>>(okResult.Value);
      Assert.Equal(count, items.Count);
    }
    #endregion

    #region GetMovie(id)
    [Fact]
    public void Get_ById_UnknownIdPassed_ReturnsNotFoundResult()
    {
      // Act
      var notFoundResult = _movieController.GetMovie(0, null);

      // Assert
      Assert.IsType<NotFoundObjectResult>(notFoundResult);
    }
    //[Fact]
    //public void GetById_WrongFields_ShouldFail()
    //{
    //  // Arrange      
    //  List<Movie> movies = _context.Movie.ToList();
    //  string fields = "FirstName";
      
    //  // Act
    //  var badRequest = _movieController.GetMovie(movies[0].Id, fields);

    //  // Assert
    //  Assert.IsType<BadRequestResult>(badRequest);
    //}
    
    //[Fact]
    //public void GetById_WithFields_ReturnsOkResult()
    //{
    //  // Arrange      
    //  List<Movie> movies = _context.Movie.ToList();
    //  string fields = "Id,Title";

    //  // Act
    //  var okResult = _movieController.GetMovie(movies[0].Id, fields);

    //  // Assert
    //  Assert.IsType<OkObjectResult>(okResult);
    //}

    //[Fact]
    //public void GetById_WithFields_ReturnsCorrectFields()
    //{
    //  // Arrange      
    //  List<Movie> movies = _context.Movie.ToList();
    //  Movie movieToUse = movies[0];
    //  string fields = "Id,Title";

    //  // Act
    //  var okResult = _movieController.GetMovie(movieToUse.Id, fields) as OkObjectResult;

    //  // Assert
    //  Assert.IsType<MovieDto>(okResult.Value);
    //  Assert.Equal(movieToUse.Id, (okResult.Value as MovieDto).Id);
    //}
    [Theory]
    [InlineData(0)]
    [InlineData(13)]
    public void Get_ById_UnknownIdPassed_ReturnsCorrectMessage(int id)
    {
      // Arrange
      // Act
      var notFoundResult = _movieController.GetMovie(id, null) as ObjectResult;

      // Assert
      Assert.Equal("Invalid Id", notFoundResult.Value);
    }

    [Fact]
    public void Get_ById_ExistingIdPassed_ReturnsOkResult()
    {
      // Arrange
      List<Movie> movies = _context.Movie.ToList();
      Movie movieToUse  = movies[0];
      // Act
      var okResult = _movieController.GetMovie(movieToUse.Id, null);

      // Assert
      Assert.IsType<OkObjectResult>(okResult);
    }
    [Fact]
    public void Get_ById_ExistingIdPassed_ReturnsRightItem()
    {
      // Arrange
      List<Movie> movies = _context.Movie.ToList();
      Movie movieToUse = movies[0];

      // Act
      var okResult = _movieController.GetMovie(movieToUse.Id, null) as OkObjectResult;
      var movie = okResult.Value;
      var movieAsDictionary = movie as IDictionary<string, object>;

      // Assert
      Assert.IsType <System.Dynamic.ExpandoObject> (movie);      
      Assert.Equal(movieToUse.Id, (int)movieAsDictionary["Id"]);
    }
    #endregion

    #region CreateMovie(MovieForCreationDto movie) 
    [Fact]
    public void Add_InvalidObjectPassed_ReturnsBadRequest()
    {
      // Arrange
      var titleMissingItem = new MovieForCreationDto();
      var result = new List<ValidationResult>();
      
      // Act
      var isValid = Validator.TryValidateObject(titleMissingItem, new System.ComponentModel.DataAnnotations.ValidationContext(titleMissingItem), result );
      
      // Assert
      Assert.False(isValid);
      Assert.Equal(1, result.Count);
      Assert.Equal("Title", result[0].MemberNames.ElementAt(0));
      Assert.Equal("You should fill out a title.", result[0].ErrorMessage);

    }
    [Fact]
    public void Add_ValidObjectPassed_ReturnsCreatedResponse()
    {
      // Arrange
      MovieForCreationDto testItem = new MovieForCreationDto()
      {
        Title = "new movie"
      };
      //TryValidateModel requires some Mocks
      var objectValidator = new Mock<IObjectModelValidator>();
      objectValidator.Setup(o => o.Validate(It.IsAny<ActionContext>(),
                                        It.IsAny<ValidationStateDictionary>(),
                                        It.IsAny<string>(),
                                        It.IsAny<Object>()));
      _movieController.ObjectValidator = objectValidator.Object;

      // Act
      var createdResponse = _movieController.CreateMovie(testItem);

      // Assert      
      Assert.IsType<CreatedAtRouteResult>(createdResponse.Result);
    }
    [Fact]
    public void Add_ValidObjectPassed_ReturnedResponseHasCreatedItem()
    {
      // Arrange
      string newTitle = "new added movie";
      string newGenre = "superherodc";
      MovieForCreationDto testItem = new MovieForCreationDto()
      {
        Title = newTitle,
        Genre = newGenre
      };
      //TryValidateModel requires some Mocks
      var objectValidator = new Mock<IObjectModelValidator>();
      objectValidator.Setup(o => o.Validate(It.IsAny<ActionContext>(),
                                        It.IsAny<ValidationStateDictionary>(),
                                        It.IsAny<string>(),
                                        It.IsAny<Object>()));
      _movieController.ObjectValidator = objectValidator.Object;

      // Act
      var createdResponse = _movieController.CreateMovie(testItem);
      var item = ((ObjectResult)createdResponse.Result).Value as MovieDto;

      // Assert
      Assert.IsType<MovieDto>(item);
      Assert.Equal(newTitle, item.Title);
      Assert.Equal(newGenre.ToUpper(), item.Genre.ToUpper());
    }
    [Fact]
    public void Add_TitleOnly_ReturnedResponseHasCreatedItem()
    {
      // Arrange
      string newTitle = "new added movie";
      MovieForCreationDto testItem = new MovieForCreationDto()
      {
        Title = newTitle
      };
      //TryValidateModel requires some Mocks
      var objectValidator = new Mock<IObjectModelValidator>();
      objectValidator.Setup(o => o.Validate(It.IsAny<ActionContext>(),
                                        It.IsAny<ValidationStateDictionary>(),
                                        It.IsAny<string>(),
                                        It.IsAny<Object>()));
      _movieController.ObjectValidator = objectValidator.Object;

      // Act
      var createdResponse = _movieController.CreateMovie(testItem);
      var item = ((ObjectResult)createdResponse.Result).Value as MovieDto;

      // Assert
      Assert.IsType<MovieDto>(item);
      Assert.Equal(newTitle, item.Title);
      Assert.Equal(MovieGenre.Unknown.ToString().ToUpper(), item.Genre.ToUpper());
    }
    #endregion

    #region UpdateMovie(int id, MovieForUpdateDto movie)
    [Fact]
    public void Update_InvalidObjectPassed_ReturnsBadRequest()
    {
      // Arrange
      var titleMissingItem = new MovieForUpdateDto();
      var result = new List<ValidationResult>();

      // Act
      var isValid = Validator.TryValidateObject(titleMissingItem, new System.ComponentModel.DataAnnotations.ValidationContext(titleMissingItem), result);

      // Assert
      Assert.False(isValid);
      Dictionary<string,string> validation = new Dictionary<string, string>() { 
        { "Title", "You should fill out a title." }, { "Genre", "You should fill out a genre." } };
      Assert.Equal(validation.Count, result.Count);
      
    }
    [Fact]
    public void Update_ValidObjectPassed_ReturnsCreatedResponse()
    {
      // Arrange
      string newTitle = "Updated Title";
      string newGenre = "superherodc";
      MovieForUpdateDto testItem = new MovieForUpdateDto()
      {
        Title = newTitle,
        Genre = newGenre
      };
      List<Movie> movies = _context.Movie.ToList();
      Movie movieToUse = movies[0];

      //TryValidateModel requires some Mocks
      var objectValidator = new Mock<IObjectModelValidator>();
      objectValidator.Setup(o => o.Validate(It.IsAny<ActionContext>(),
                                        It.IsAny<ValidationStateDictionary>(),
                                        It.IsAny<string>(),
                                        It.IsAny<Object>()));
      _movieController.ObjectValidator = objectValidator.Object;

      // Act
      var createdResponse = _movieController.UpdateMovie(movieToUse.Id, testItem);

      // Assert      
      Assert.IsType<NoContentResult> (createdResponse);
    }
    [Fact]
    public void Update_ValidObjectPassed_ReturnedResponseHasCreatedItem()
    {
      // Arrange
      string newTitle = "Updated Title 2";
      string newGenre = MovieGenre.SuperHeroDC.ToString();
      List<Movie> movies = _context.Movie.ToList();
      Movie movieToUse = movies[0];
      MovieForUpdateDto testItem = new MovieForUpdateDto()
      {
        Title = newTitle,
        Genre = newGenre
      };
      //TryValidateModel requires some Mocks
      var objectValidator = new Mock<IObjectModelValidator>();
      objectValidator.Setup(o => o.Validate(It.IsAny<ActionContext>(),
                                        It.IsAny<ValidationStateDictionary>(),
                                        It.IsAny<string>(),
                                        It.IsAny<Object>()));
      _movieController.ObjectValidator = objectValidator.Object;

      // Act
      var createdResponse = _movieController.UpdateMovie(movieToUse.Id, testItem);
      var okResult = _movieController.GetMovie(movieToUse.Id, null) as OkObjectResult;
      var movieAfterUpdate = okResult.Value;
      var movieAsDictionary = movieAfterUpdate as IDictionary<string, object>;

      // Assert
      Assert.IsType<System.Dynamic.ExpandoObject>(okResult.Value);
      Assert.Equal(newTitle, (string)movieAsDictionary["Title"]);
      Assert.Equal(newGenre.ToUpper(), movieAsDictionary["Genre"].ToString().ToUpper());
    }
    [Fact]
    public void Update_TitleGenre_ReturnedResponseHasCreatedItem()
    {
      // Arrange
      string newTitle = "Updated Title 1111111";
      string newGenre = MovieGenre.ScienceFiction.ToString();
      List<Movie> movies = _context.Movie.ToList();
      Movie movieToUse = movies[0];
      MovieForUpdateDto testItem = new MovieForUpdateDto()
      {
        Title = newTitle,
        Genre = newGenre
      };
      //TryValidateModel requires some Mocks
      var objectValidator = new Mock<IObjectModelValidator>();
      objectValidator.Setup(o => o.Validate(It.IsAny<ActionContext>(),
                                        It.IsAny<ValidationStateDictionary>(),
                                        It.IsAny<string>(),
                                        It.IsAny<Object>()));
      _movieController.ObjectValidator = objectValidator.Object;

      // Act
      var createdResponse = _movieController.UpdateMovie(movieToUse.Id, testItem);
      movies = _context.Movie.ToList();
      var okResult = _movieController.GetMovie(movieToUse.Id, null) as OkObjectResult;
      var movieAfterUpdate = okResult.Value;
      var movieAsDictionary = movieAfterUpdate as IDictionary<string, object>;

      // Assert
      Assert.IsType<System.Dynamic.ExpandoObject>(okResult.Value);
      Assert.Equal(newTitle, (string)movieAsDictionary["Title"]);
      Assert.Equal(newGenre.ToUpper(), movieAsDictionary["Genre"].ToString().ToUpper());
    }
    #endregion

    #region DeleteMovie(int id)
    [Theory]
    [InlineData(0)]
    [InlineData(13)]
    public void Remove_NotExistingIddPassed_ReturnsNotFoundResponse(int id)
    {
      // Arrange      

      // Act
      var badResponse = _movieController.DeleteMovie(id);

      // Assert
      Assert.IsType<NotFoundResult>(badResponse);
    }
    [Fact]
    public void Remove_ExistingIdPassed_RemovesOneItem()
    {
      // Arrange
      List<Movie> movies = _context.Movie.ToList();
      Movie movieToUse = movies[0];

      // Act
      var countBefore = ((List<System.Dynamic.ExpandoObject>)((OkObjectResult)_movieController.GetMovies(_movieResourceParameters)).Value).Count;
      var okResponse = _movieController.DeleteMovie(movieToUse.Id);
      _movieController.ControllerContext.HttpContext.Response.Headers.Clear();
      var countAfter = ((List<System.Dynamic.ExpandoObject>)((OkObjectResult)_movieController.GetMovies(_movieResourceParameters)).Value).Count;

      // Assert
      Assert.Equal(countBefore, countAfter + 1);
    }
    #endregion


    //test:
    //public ActionResult GetMovie(int id, string fields)
    //{
    //  if (!_propertyCheckerService.TypeHasProperties<MovieDto>
    //           (fields))

    //    GetMovie(ShapeData
    //    GetMovies(ShapeData

  }
}
